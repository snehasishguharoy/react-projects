import { useState } from 'react';
import './Counter.css';

import {PropTypes} from 'prop-types';
import CounterButton from './CounterButton';

export default function Counter(){
  
  const [count,setCount]= useState(0);

  function incrementCounterParent(by){
    setCount(count+by);
  }
  function decrementCounterParent(by){
    setCount(count-by);
  }
  function resetCounter(){
    setCount(0);
  }

return (
  <>
    <span className="totalcount">{count}</span>
    <CounterButton incrementMethod={incrementCounterParent} decrementMethod={decrementCounterParent} by={1}/>
    <CounterButton incrementMethod={incrementCounterParent} decrementMethod={decrementCounterParent} by={2}/>
    <CounterButton incrementMethod={incrementCounterParent} decrementMethod={decrementCounterParent} by={3}/>
    <button className="resetButton" onClick={resetCounter}>Reset</button>
  </>

)

}


