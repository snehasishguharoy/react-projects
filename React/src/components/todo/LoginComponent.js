import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useAuth } from "./security/AuthContext";

export default function LoginComponent() {
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [successMessage, setSuccessMessage] = useState(false);
  const [errorMessage, setErrorMessage] = useState(false);
  const navigate = useNavigate();

  const authCtx = useAuth();

  function handleUserNameChange(event) {
    setUserName(event.target.value);
  }
  function handlePasswordChange(event) {
    setPassword(event.target.value);
    console.log(event.target.value);
  }
  async function handleSubmit() {
    console.log(username);
    console.log(password);
    if (await authCtx.login(username, password)) {
      navigate(`/welcome/${username}`);
    } else {
      setErrorMessage(true);
    }
  }
  return (
    <div className="Login">
      {errorMessage && (
        <div className="errorMessage">
          Authenticated Failed. Please check your credentials
        </div>
      )}
      <h1>Time to Login</h1>

      <div className="LoginForm">
        <div>
          <label>User Name: </label>
          <input
            type="text"
            name="username"
            value={username}
            onChange={handleUserNameChange}
          />
        </div>
        <div>
          <label>Password: </label>
          <input
            type="password"
            name="password"
            value={password}
            onChange={handlePasswordChange}
          />
        </div>
        <div>
          <button className="btn btn-success m-5" type="button" name="login" onClick={handleSubmit}>
            Login
          </button>
        </div>
      </div>
    </div>
  );
}
