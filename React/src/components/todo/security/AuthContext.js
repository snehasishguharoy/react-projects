// Create a Context
// Put some state in the context
// Share the created context with other components

import { createContext, useContext, useState } from "react";
import { apiClient } from "../api/ApiClient";
import { executeBasicAuthenticationService, executejwtAuthenticationService } from "../api/AuthenticationService";

const AuthContext = createContext();
export const useAuth = () => useContext(AuthContext);

function AuthProvider({ children }) {
  const [isAuthenticated, setAuthenticated] = useState(false);
  const [username,setUserName]=useState(null);
  const [token,setToken]=useState(null);

//   function login(username, password) {
//     if (username === "Snehasish" && password === "Java@786") {
//       setAuthenticated(true);
//       setUserName(username);
//       return true;
//     } else {
//       setAuthenticated(false);
//       setUserName(null);
//       return false;
//     }
//   }



//   async function login(username, password) {
//     const baToken='Basic '+window.btoa(username+":"+password);
//     try{
//         const response=await executeBasicAuthenticationService(baToken);
//         if(response.status==200){
//             setAuthenticated(true);
//             setUserName(username);
//             setToken(baToken);
//             apiClient.interceptors.request.use((config)=>{
//                 console.log("intercepting and adding a token");
//                 config.headers.Authorization=baToken;
//                 return config;
//             })


//             return true;
//         }
//         else{
//             logout();
//             return false;
//         }
//     }
//     catch(err){
//         logout();
//         return false;
//     }
   
//   }
  async function login(username, password) {
    try{
        console.log(username);
        console.log(password);
        const response=await executejwtAuthenticationService(username,password);
        if(response.status==200){
            setAuthenticated(true);
            const jwtToken='Bearer '+response.data.token;
            setUserName(username);
            setToken(jwtToken);
            apiClient.interceptors.request.use((config)=>{
                console.log("intercepting and adding a token");
                config.headers.Authorization=jwtToken;
                return config;
            })


            return true;
        }
        else{
            logout();
            return false;
        }
    }
    catch(err){
        logout();
        return false;
    }
   
  }


  function logout() {
    setAuthenticated(false);
    setToken(null);
    setUserName(null);
  }

  return (
    <AuthContext.Provider value={{ isAuthenticated, login, logout,username,token }}>
      {children}
    </AuthContext.Provider>
  );
}
export default AuthProvider;
