import axios from "axios";

export const apiClient = axios.create({
  // baseURL: "http://localhost:5000",
  baseURL:
    "http://fullstackapplication-env.eba-j9pir8up.ap-south-1.elasticbeanstalk.com/",
});
