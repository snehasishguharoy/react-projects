import axios from "axios";
import { apiClient } from "./ApiClient";

export const executeBasicAuthenticationService = (token) =>
  apiClient.get(`/basic-auth`, {
    headers: {
      Authorization: token,
    },
  });

export const executejwtAuthenticationService = (username, password) =>
  apiClient.post(`/authenticate`, {
    username: username,
    password: password,
  });
