import { useParams, Link } from "react-router-dom";
import axios from "axios";
import { useState } from "react";
import {retrieveHelloWorldBean,retrieveHelloWorldPathVariable} from "./api/HelloWorldApiService";
import { useAuth } from "./security/AuthContext";


 

export default function WelcomeComponent() {
  const { username } = useParams();
  const [msg, setMsg] = useState(null);
  const authCtx=useAuth();


  async function callHelloWorldRestAPI() {
  
  const res=await retrieveHelloWorldPathVariable(username)
      if(res.status==200){
        successfulResponse(res);
      }
  }
  function successfulResponse(response) {
    console.log(response);
    setMsg(response.data.msg);
  }
  function errorResponse(err) {
    console.log(err);
  }
  return (
    <div className="WelcomeComponent">
      <div>
        <h1>Welcome {username}</h1>
      </div>
      Manage Your Todos - <Link to="/todos">Go Here</Link>
      <div>
        <button className="btn btn-success m-5" onClick={callHelloWorldRestAPI}>
          Call Hello World
        </button>
      </div>
      <div className="text-info">{msg}</div>
    </div>
  )
}

