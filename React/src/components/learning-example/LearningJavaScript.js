const person={

    name: "Snehasish Guha Roy",
    address: {
        line1: "College Street",
        city: "Kolkata",
        country: "India"
    },
    profiles: ['twitter','linkedin','insta'],
    printProfile: ()=>{
        
        person.profiles.map(prof=>
            console.log(prof)
        )
    }
};

function LearningJavaScript(){

return (
    <>
    <div>{person.name}</div>
    <div>{person.address.city}</div>
    <div>{person.address.line1}</div>
    <div>{person.address.country}</div>
    <div>{person.profiles[0]}</div>
    <div>{person.profiles[1]}</div>
    <div>{person.profiles[2]}</div>
    <div>{person.printProfile()}</div>
    </>

)

}

export default LearningJavaScript;