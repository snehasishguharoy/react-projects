

import './App.css';
import Counter from './components/counter/Counter';
import TodoApp from './components/todo/TodoApp';



function App() {
  return ( <div className='App'>
    {/* <LearningComponent/> */}
    {/* <PlayingWithProperties property1="value1" property2="value2"/> */}
      {/* <Counter/> */}
      <TodoApp/>
    </div>)
  
}

// function PlayingWithProperties(props){
//   console.log(props.property1)
//   console.log(props.property2)
//   return (
//     <div>Properties</div>
//   )


// }
export default App;
